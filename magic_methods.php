<?php
//__construct()
class person{
    public $name="saima", $phone="1234", $dateofbirth="24feb";
    public static $message;
    //first in first execute
    public function __construct()
    {
        echo "i am inside the" . __METHOD__ . "<br>";
    }
    //last in first execute
    public function __destruct()
    {
        echo "i am dying inside the" . __METHOD__ . "<br>";
    }

    public function __call($name, $arguments)
    {
        echo "i am inside the" . __METHOD__ . "<br>";
        echo "name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }
    public static function dosomething()
    {
        echo " i am doing something<br>";
    }
    public function __set($name, $value)
    {
        echo __METHOD__. " wrong property name = $name<br>";
        echo __METHOD__." wrong property value set = $value<br>";
    }
    public function __get($name)
    {
        echo __METHOD__." get wrong property name<br>";
    }
    public function __isset($name)
    {
        echo __METHOD__. " wrong  name = $name<br>";

    }
    public function __unset($name)
    {
        echo __METHOD__. " wrong  name 1 = $name<br>";

    }
    public function __sleep()
    {
        return array("name","phone");
    }
    public function __wakeup()

    {
        echo "<br> i am inside wakeup<br>";
        $this->dosomething();
    }
    public function __invoke()
    {
       echo "<br>you are object<br>";
    }
}
class person1{
    public $name;
    //first in first execute
    public function __construct()
    {
        echo "i am inside the" . __METHOD__ . "<br>";
    }
    //last in first execute
    public function __destruct()
    {
        echo "i am dying inside the" . __METHOD__ . "<br>";
    }
}

person::$message= "any message<br>";
echo person::$message;
person::dosomething();
$obj = new person1();
unset ($obj);
$obj = new person();
$obj->hijibiji("pera_habi1","pera_jabbi1");
$obj->address="5/2 gol pahar mor, chittagong";
echo $obj->address;
if(isset($obj->naiproperty))
{
//isset will be called
}
unset($obj->naiproperty);//unset will be called

$myvar = serialize($obj);
echo "<pre>";
var_dump($myvar);//N means value null
echo "</pre>";
var_dump(unserialize($myvar));//N means value null
echo $obj();